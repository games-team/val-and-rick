/*
 * $Id: crystal.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.crystal;

private import std.math;
private import opengl;
private import abagames.util.actor;
private import abagames.util.vector;
private import abagames.vr.field;
private import abagames.vr.ship;
private import abagames.vr.screen;
private import abagames.vr.shape;

/**
 * Bonus crystals.
 */
public class Crystal: Actor {
 private:
  static const int COUNT = 60;
  static BitmapShape _shape;
  Field field;
  Ship ship;
  Vector pos;
  Vector vel;
  int cnt;

  public static void init() {
    _shape = new BitmapShape(BitmapShape.ground, 19, 0.75f);
  }

  public static void close() {
    _shape.close();
  }

  public override void init(Object[] args) {
    field = cast(Field) args[0];
    ship = cast(Ship) args[1];
    pos = new Vector;
    vel = new Vector;
  }

  public void set(Vector p) {
    pos.x = p.x;
    pos.y = p.y;
    cnt = COUNT;
    vel.x = 0;
    vel.y = 0.1f;
    exists = true;
  }

  public override void move() {
    cnt--;
    float dist = pos.dist(ship.pos);
    if (cnt < 0 || dist < 2) {
      ship.getCrystal();
      exists = false;
      return;
    }
    if (cnt < cast(int) COUNT * 0.8f) {
      vel.x += (ship.pos.x - pos.x) / dist * 0.07f;
      vel.y += (ship.pos.y - pos.y) / dist * 0.07f;
    }
    vel *= 0.95f;
    pos += vel;
  }

  public override void draw() {
    glPushMatrix();
    Screen.glTranslate(pos);
    _shape.draw();
    glPopMatrix();
  }

  public static BitmapShape shape() {
    return _shape;
  }
}

public class CrystalPool: ActorPool!(Crystal) {
  public this(int n, Object[] args) {
    super(n, args);
  }
}
