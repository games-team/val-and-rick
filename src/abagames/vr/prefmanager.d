/*
 * $Id: prefmanager.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.prefmanager;

private import std.stream;
private import abagames.util.prefmanager;
private import abagames.vr.ship;

/**
 * Save/Load the high score.
 */
public class PrefManager: abagames.util.prefmanager.PrefManager {
 private:
  static const int VERSION_NUM = 10;
  static const char[] PREF_FILE = "vr.prf";
  PrefData _prefData;

  public this() {
    _prefData = new PrefData;
  }

  public void load() {
    auto File fd = new File;
    try {
      int ver;
      fd.open(PREF_FILE);
      fd.read(ver);
      if (ver != VERSION_NUM)
        throw new Error("Wrong version num");
      _prefData.load(fd);
    } catch (Object e) {
      _prefData.init();
    } finally {
      if (fd.isOpen())
        fd.close();
    }
  }

  public void save() {
    auto File fd = new File;
    fd.create(PREF_FILE);
    fd.write(VERSION_NUM);
    _prefData.save(fd);
    fd.close();
  }

  public PrefData prefData() {
    return _prefData;
  }
}

public class PrefData {
 private:
  float _hiDistance;

  public this() {
  }

  public void init() {
    _hiDistance = 0;
  }

  public void load(File fd) {
    fd.read(_hiDistance);
  }

  public void save(File fd) {
    fd.write(_hiDistance);
  }

  public void recordResult(float dist) {
    if (dist > _hiDistance)
      _hiDistance = dist;
  }

  public float hiDistance() {
    return _hiDistance;
  }
}
