/*
 * $Id: soundmanager.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.soundmanager;

private import std.path;
private import std.file;
private import abagames.util.rand;
private import abagames.util.logger;
private import abagames.util.sdl.sound;

/**
 * Manage BGMs and SEs.
 */
public class SoundManager: abagames.util.sdl.sound.SoundManager {
 private static:
  char[][] seFileName =
    ["shot.wav", "zako_dest.wav", "large_dest.wav", "ship_dest.wav", "blaster.wav",
     "crystal.wav", "rocket.wav"];
  int[] seChannel =
    [0, 1, 2, 3, 4, 5, 6, 7];
  Music[char[]] bgm;
  Chunk[char[]] se;
  bool bgmDisabled = false;
  bool seDisabled = false;

  public static void loadSounds() {
    bgm = loadMusics();
    se = loadChunks();
  }

  private static Music[char[]] loadMusics() {
    Music[char[]] musics;
    char[][] files = listdir(Music.dir);
    foreach (char[] fileName; files) {
      char[] ext = getExt(fileName);
      if (ext != "ogg" && ext != "wav")
        continue;
      Music music = new Music();
      music.load(fileName);
      musics[fileName] = music;
      Logger.info("Load bgm: " ~ fileName);
    }
    return musics;
  }

  private static Chunk[char[]] loadChunks() {
    Chunk[char[]] chunks;
    int i = 0;
    foreach (char[] fileName; seFileName) {
      Chunk chunk = new Chunk();
      chunk.load(fileName, seChannel[i]);
      chunks[fileName] = chunk;
      Logger.info("Load SE: " ~ fileName);
      i++;
    }
    return chunks;
  }

  public static void playBgm(char[] name) {
    if (bgmDisabled)
      return;
    Music.haltMusic();
    bgm[name].play();
  }

  public static void fadeBgm() {
    Music.fadeMusic();
  }

  public static void haltBgm() {
    Music.haltMusic();
  }

  public static void playSe(char[] name) {
    if (seDisabled)
      return;
    se[name].play();
  }

  public static void disableSe() {
    seDisabled = true;
  }

  public static void enableSe() {
    seDisabled = false;
  }

  public static void disableBgm() {
    bgmDisabled = true;
  }

  public static void enableBgm() {
    bgmDisabled = false;
  }
}
