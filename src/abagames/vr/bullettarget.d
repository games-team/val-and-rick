/*
 * $Id: bullettarget.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.bullettarget;

private import abagames.util.vector;

/**
 * Target that is aimed by bullets.
 */
public interface BulletTarget {
 public:
  Vector getTargetPos();
}

public class VirtualBulletTarget: BulletTarget {
 public:
  Vector pos;
 private:

  public this() {
    pos = new Vector;
  }

  public Vector getTargetPos() {
    return pos;
  }
}
