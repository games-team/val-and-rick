/*
 * $Id: particle.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.particle;

private import std.math;
private import opengl;
private import abagames.util.actor;
private import abagames.util.vector;
private import abagames.util.rand;
private import abagames.util.sdl.luminous;
private import abagames.vr.field;
private import abagames.vr.screen;

/**
 * Particles.
 */
public class Particle: LuminousActor {
 private:
  static const float GRAVITY = 0.02;
  static const float SIZE = 0.3;
  static Rand rand;
  Field field;
  Vector3 pos, ppos;
  Vector3 vel;
  float r, g, b;
  float lumAlp;
  int cnt;

  public static this() {
    rand = new Rand;
  }

  public static void setRandSeed(long seed) {
    rand.setSeed(seed);
  }

  public override void init(Object[] args) {
    field = cast(Field) args[0];
    pos = new Vector3;
    ppos = new Vector3;
    vel = new Vector3;
  }

  public void set(Vector p, float vx, float vy, float r, float g, float b, int c) {
    ppos.x = pos.x = p.x;
    ppos.y = pos.y = p.y;
    vel.x = vx;
    vel.y = vy;
    this.r = r;
    this.g = g;
    this.b = b;
    cnt = c;
    exists = true;
  }

  public override void move() {
    cnt--;
    if (cnt < 0) {
      exists = false;
      return;
    }
    ppos.x = pos.x;
    ppos.y = pos.y;
    pos += vel;
    vel *= 0.96f;
  }

  public override void draw() {
    glBegin(GL_TRIANGLE_FAN);
    float ox = pos.x - ppos.x;
    float oy = pos.y - ppos.y;
    ox *= 0.5f;
    oy *= 0.5f;
    Screen.setColor(r, g, b, 1);
    Screen.glVertex(ppos);
    Screen.setColor(r * 0.5f, g * 0.5f, b * 0.5f, 0);
    glVertex3f(pos.x - oy, pos.y + ox, 0);
    glVertex3f(pos.x + oy, pos.y - ox, 0);
    glEnd();
    Screen.setColor(r, g, b, 1);
    glBegin(GL_LINES);
    glVertex3f(pos.x - oy, pos.y + ox, 0);
    Screen.glVertex(ppos);
    glVertex3f(pos.x + oy, pos.y - ox, 0);
    glEnd();
  }

  public override void drawLuminous() {
    float ox = pos.x - ppos.x;
    float oy = pos.y - ppos.y;
    ox *= 0.5f;
    oy *= 0.5f;
    Screen.setColor(r, g, b, 1);
    glBegin(GL_LINES);
    glVertex3f(pos.x - oy, pos.y + ox, 0);
    Screen.glVertex(ppos);
    glVertex3f(pos.x + oy, pos.y - ox, 0);
    glEnd();
  }
}

public class ParticlePool: LuminousActorPool!(Particle) {
  public this(int n, Object[] args) {
    super(n, args);
  }
}
