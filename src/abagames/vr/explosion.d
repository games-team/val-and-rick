/*
 * $Id: explosion.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.explosion;

private import std.math;
private import opengl;
private import abagames.util.actor;
private import abagames.util.vector;
private import abagames.vr.field;
private import abagames.vr.ship;
private import abagames.vr.screen;
private import abagames.vr.shape;

/**
 * Expolsion.
 */
public class Explosion: Actor {
 private:
  static const int COUNT = 20;
  static BitmapShape[] shape;
  Field field;
  Vector pos;
  Vector vel;
  float size;
  int cnt, startCnt;

  public static void init() {
    shape ~= new BitmapShape(BitmapShape.explosions, 0);
    shape ~= new BitmapShape(BitmapShape.explosions, 1);
    shape ~= new BitmapShape(BitmapShape.explosions, 2);
    shape ~= new BitmapShape(BitmapShape.explosions, 3);
    shape ~= new BitmapShape(BitmapShape.explosions, 4);
    shape ~= new BitmapShape(BitmapShape.explosions, 5);
  }

  public static void close() {
    foreach (BitmapShape bs; shape)
      bs.close();
  }

  public override void init(Object[] args) {
    field = cast(Field) args[0];
    pos = new Vector;
    vel = new Vector;
  }

  public void set(Vector p, float mx, float my, int c = COUNT, float sz = 2) {
    pos.x = p.x;
    pos.y = p.y;
    vel.x = mx;
    vel.y = my;
    startCnt = cnt = c;
    size = sz;
    exists = true;
  }

  public override void move() {
    cnt--;
    if (cnt < 0) {
      exists = false;
      return;
    }
    vel *= 0.95f;
    pos += vel;
  }

  public override void draw() {
    glPushMatrix();
    Screen.glTranslate(pos);
    glScalef(size, size, 1);
    int si = (startCnt - cnt) / ((startCnt / 6) + 1);
    shape[si].draw();
    glPopMatrix();
  }
}

public class ExplosionPool: ActorPool!(Explosion) {
  public this(int n, Object[] args) {
    super(n, args);
  }
}
