/*
 * $Id: shape.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.shape;

private import std.string;
private import std.math;
private import opengl;
private import abagames.util.vector;
private import abagames.util.rand;
private import abagames.util.sdl.screen3d;
private import abagames.util.sdl.displaylist;
private import abagames.util.sdl.texture;
private import abagames.vr.screen;

/**
 * Interface for drawing a shape.
 */
public interface Drawable {
  public void draw();
}

/**
 * Interface and implmentation for shape that has a collision.
 */
public interface Collidable {
  public Vector collision();
  public bool checkCollision(float ax, float ay, Collidable shape = null);
}

private template CollidableImpl() {
  public bool checkCollision(float ax, float ay, Collidable shape = null) {
    float cx, cy;
    if (shape) {
      cx = collision.x + shape.collision.x;
      cy = collision.y + shape.collision.y;
    } else {
      cx = collision.x;
      cy = collision.y;
    }
    if (ax <= cx && ay <= cy)
      return true;
    else
      return false;
  }
}

/**
 * Drawable that has a single displaylist.
 */
public abstract class DrawableShape: Drawable {
  protected DisplayList displayList;
 private:

  public this() {
    displayList = new DisplayList(1);
    displayList.beginNewList();
    createDisplayList();
    displayList.endNewList();
  }

  protected abstract void createDisplayList();

  public void close() {
    displayList.close();
  }

  public void draw() {
    displayList.call(0);
  }
}

/**
 * Drawable that has a collision.
 */
public abstract class CollidableDrawable: DrawableShape, Collidable {
  mixin CollidableImpl;
  protected Vector _collision;
 private:

  public this() {
    super();
    setCollision();
  }

  protected abstract void setCollision();

  public Vector collision() {
    return _collision;
  }
}

/**
 * Drawable that can change a size.
 */
public class ResizableDrawable: Drawable, Collidable {
  mixin CollidableImpl;

 private:
  Drawable _shape;
  float _size;
  Vector _collision;

  public void draw() {
    glScalef(_size, _size, _size);
    _shape.draw();
  }

  public Drawable shape(Drawable v) {
    _collision = new Vector;
    return _shape = v;
  }

  public float size(float v) {
    return _size = v;
  }

  public float size() {
    return _size;
  }

  public Vector collision() {
    Collidable cd = cast(Collidable) _shape;
    if (cd) {
      _collision.x = cd.collision.x * _size;
      _collision.y = cd.collision.y * _size;
      return _collision;
    } else {
      return null;
    }
  }
}

/**
 * Bitmap shape.
 */
public class BitmapShape: Drawable, Collidable {
  mixin CollidableImpl;
 public:
  static Texture letters;
  static Texture enemies;
  static Texture bullets;
  static Texture ground;
  static Texture ship;
  static Texture purser;
  static Texture explosions;
  static Texture sight;
  static Texture ascii;
 private:
  static const float TEXTURE_SIZE_MIN = 0.02f;
  static const float TEXTURE_SIZE_MAX = 0.98f;
  static const float BASE_SIZE = 0.5f;
  Texture texture;
  int idx;
  Vector _collision;
  float size;
  DisplayList displayList;

  public static void init() {
    letters = new Texture("all.bmp", 0, 0, 26, 3, 8, 8, 4294902015u);
    enemies = new Texture("all.bmp", 0, 72, 4, 3, 16, 16, 4294902015u);
    bullets = new Texture("all.bmp", 0, 312, 2, 2, 8, 8, 4294902015u);
    ground = new Texture("all.bmp", 32, 120, 2, 10, 16, 16, 4294902015u);
    ship = new Texture("all.bmp", 0, 24, 2, 3, 16, 16, 4294902015u);
    purser = new Texture("all.bmp", 32, 24, 1, 2, 16, 16, 4294902015u);
    explosions = new Texture("all.bmp", 0, 120, 1, 6, 32, 32, 4294902015u);
    sight = new Texture("sight.bmp", 0, 0, 1, 3, 16, 16, 4294902015u);
    ascii = new Texture("all.bmp", 384, 384, 8, 16, 8, 8, 4294902015u);
  }

  public static void closeAll() {
    letters.close();
    enemies.close();
    bullets.close();
    ground.close();
    ship.close();
    purser.close();
    explosions.close();
    sight.close();
  }

  public this(Texture texture, int idx, float size = 1) {
    this.texture = texture;
    this.idx = idx;
    _collision = new Vector(BASE_SIZE * size, BASE_SIZE * size);
    this.size = size;
    displayList = new DisplayList(2);
    displayList.beginNewList();
    drawNormShape();
    displayList.nextNewList();
    drawAlphaShape();
    displayList.endNewList();
  }

  public void close() {
    displayList.close();
  }

  private void drawNormShape() {
    if (size != 1)
      glScalef(size, size, 1);
    glBlendFunc(GL_DST_COLOR,GL_ZERO);
    Screen.setColorForced(1, 1, 1);
    texture.bindMask(idx);
    glBegin(GL_TRIANGLE_FAN);
    glTexCoord2f(TEXTURE_SIZE_MIN, TEXTURE_SIZE_MIN);
    glVertex3f(-BASE_SIZE, BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MAX, TEXTURE_SIZE_MIN);
    glVertex3f(BASE_SIZE, BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MAX, TEXTURE_SIZE_MAX);
    glVertex3f(BASE_SIZE, -BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MIN, TEXTURE_SIZE_MAX);
    glVertex3f(-BASE_SIZE, -BASE_SIZE, 0);
    glEnd();
    glBlendFunc(GL_ONE, GL_ONE);
    Screen.setColor(1, 1, 1);
    texture.bind(idx);
    glBegin(GL_TRIANGLE_FAN);
    glTexCoord2f(TEXTURE_SIZE_MIN, TEXTURE_SIZE_MIN);
    glVertex3f(-BASE_SIZE, BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MAX, TEXTURE_SIZE_MIN);
    glVertex3f(BASE_SIZE, BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MAX, TEXTURE_SIZE_MAX);
    glVertex3f(BASE_SIZE, -BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MIN, TEXTURE_SIZE_MAX);
    glVertex3f(-BASE_SIZE, -BASE_SIZE, 0);
    glEnd();
  }

  private void drawAlphaShape() {
    if (size != 1)
      glScalef(size, size, 1);
    texture.bind(idx);
    glBegin(GL_TRIANGLE_FAN);
    glTexCoord2f(TEXTURE_SIZE_MIN, TEXTURE_SIZE_MIN);
    glVertex3f(-BASE_SIZE, BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MAX, TEXTURE_SIZE_MIN);
    glVertex3f(BASE_SIZE, BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MAX, TEXTURE_SIZE_MAX);
    glVertex3f(BASE_SIZE, -BASE_SIZE, 0);
    glTexCoord2f(TEXTURE_SIZE_MIN, TEXTURE_SIZE_MAX);
    glVertex3f(-BASE_SIZE, -BASE_SIZE, 0);
    glEnd();
  }

  public void draw() {
    displayList.call(0);
  }

  public void drawAlpha(float a = 0.5f) {
    Screen.setColor(a, a, a, a);
    displayList.call(1);
  }

  public Vector collision() {
    return _collision;
  }
}
