/*
 * $Id: barrage.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.barrage;

private import std.math;
private import std.string;
private import std.path;
private import std.file;
private import bulletml;
private import abagames.util.rand;
private import abagames.util.logger;
private import abagames.vr.bulletactor;
private import abagames.vr.bulletactorpool;
private import abagames.vr.bulletimpl;
private import abagames.vr.bullettarget;
private import abagames.vr.shape;

/**
 * Barrage manager(BulletMLs' loader).
 */
public class BarrageManager {
 private:
  static BulletMLParserTinyXML *parser[char[]][char[]];
  static const char[] BARRAGE_DIR_NAME = "barrage";

  public static void load() {
    char[][] dirs = listdir(BARRAGE_DIR_NAME);
    foreach (char[] dirName; dirs) {
      char[][] files = listdir(BARRAGE_DIR_NAME ~ "/" ~ dirName);
      foreach (char[] fileName; files) {
        if (getExt(fileName) != "xml")
          continue;
        parser[dirName][fileName] = getInstance(dirName, fileName);
      }
    }
  }

  public static BulletMLParserTinyXML* getInstance(char[] dirName, char[] fileName) {
    if (!parser[dirName][fileName]) {
      char[] barrageName = dirName ~ "/" ~ fileName;
      Logger.info("Loading BulletML: " ~ barrageName);
      parser[dirName][fileName] = 
        BulletMLParserTinyXML_new(std.string.toStringz(BARRAGE_DIR_NAME ~ "/" ~ barrageName));
      BulletMLParserTinyXML_parse(parser[dirName][fileName]);
    }
    return parser[dirName][fileName];
  }

  public static BulletMLParserTinyXML*[] getInstanceList(char[] dirName) {
    BulletMLParserTinyXML *pl[];
    foreach (BulletMLParserTinyXML *p; parser[dirName]) {
      pl ~= p;
    }
    return pl;
  }

  public static void unload() {
    foreach (BulletMLParserTinyXML *pa[char[]]; parser) {
      foreach (BulletMLParserTinyXML *p; pa) {
        BulletMLParserTinyXML_delete(p);
      }
    }
  }
}
