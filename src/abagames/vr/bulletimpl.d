/*
 * $Id: bulletimpl.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.bulletimpl;

private import bulletml;
private import abagames.util.bulletml.bullet;
private import abagames.vr.bulletactor;
private import abagames.vr.bullettarget;
private import abagames.vr.shape;

/**
 * Params of bullet.
 */
public class BulletImpl: Bullet {
 public:
  ParserParam[] parserParam;
  int parserIdx;
  float xReverse, yReverse;
  float baseSpeed;
  BulletTarget target;
  BulletActor rootBullet;
  bool isTopBullet;
  int shape;
 private:

  public this(int id) {
    super(id);
  }

  public void setParamFirst(ParserParam[] parserParam,
                            float xReverse, float yReverse, float baseSpeed,
                            BulletTarget target, BulletActor rootBullet, int shape = 0) {
    this.parserParam = parserParam;
    this.xReverse = xReverse;
    this.yReverse = yReverse;
    this.baseSpeed = baseSpeed;
    this.target = target;
    this.rootBullet = rootBullet;
    this.shape = shape;
    isTopBullet = true;
    parserIdx = 0;
  }

  public void setParam(BulletImpl bi) {
    parserParam = bi.parserParam;
    xReverse = bi.xReverse;
    yReverse = bi.yReverse;
    baseSpeed = bi.baseSpeed;
    target = bi.target;
    rootBullet = bi.rootBullet;
    shape = bi.shape;
    parserIdx = bi.parserIdx;
    isTopBullet = false;
  }

  public void addParser(BulletMLParser *p, float r, float re, float s) {
    parserParam ~= new ParserParam(p, r, re, s);
  }
  
  public bool gotoNextParser() {
    parserIdx++;
    if (parserIdx >= parserParam.length) {
      parserIdx--;
      return false;
    } else {
      return true;
    }
  }

  public BulletMLParser* getParser() {
    return parserParam[parserIdx].parser;
  }
  
  public void resetParser() {
    parserIdx = 0;
  }

  public override float rank() {
    ParserParam pp = parserParam[parserIdx];
    float r = pp.rank + (rootBullet.rootRank - 1) * pp.rootRankEffect * pp.rank;
    //float r = pp.rank;
    if (r > 1)
      r = 1;
    return r;
  }

  public float getSpeedRank() {
    return parserParam[parserIdx].speed * baseSpeed;
  }
}

public class ParserParam {
 public:
  BulletMLParser *parser;
  float rank;
  float rootRankEffect;
  float speed;

  public this(BulletMLParser *p, float r, float re, float s) {
    parser = p;
    rank = r;
    rootRankEffect = re;
    speed = s;
  }
}
