/*
 * $Id: title.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.title;

private import std.math;
private import opengl;
private import openglu;
private import abagames.util.vector;
private import abagames.util.sdl.displaylist;
private import abagames.util.sdl.texture;
private import abagames.util.sdl.pad;
private import abagames.vr.screen;
private import abagames.vr.prefmanager;
private import abagames.vr.field;
private import abagames.vr.letter;
private import abagames.vr.gamemanager;

/**
 * Title screen.
 */
public class TitleManager {
 private:
  static const float SCROLL_SPEED_BASE = 0.05f;
  PrefManager prefManager;
  RecordablePad pad;
  Field field;
  GameManager gameManager;
  int cnt;
  bool _replayMode;
  bool btnPressed;

  public this(PrefManager prefManager, Pad pad, Field field, GameManager gameManager) {
    this.prefManager = prefManager;
    this.pad = cast(RecordablePad) pad;
    this.field = field;
    this.gameManager = gameManager;
  }

  public void close() {
  }

  public void start() {
    cnt = 0;
    field.start();
    _replayMode = false;
    btnPressed = true;
  }

  public void move(bool hasReplayData) {
    if (!hasReplayData)
      field.scroll(SCROLL_SPEED_BASE, true);
    PadState input = pad.getState(false);
    if (input.button & PadState.Button.A) {
      if (!btnPressed) {
        bool showRank = false;
        if (input.button & PadState.Button.B)
          showRank = true;
        gameManager.startInGame(showRank);
      }
    } else {
      btnPressed = false;
    }
    cnt++;
  }

  public void draw() {
    Letter.drawString("VAL   RICK", -2.5f, 4, 1, Letter.Direction.TO_RIGHT, 1);
    Letter.drawString("&", -0.5f, 4, 1);
    if ((cnt % 64) < 32)
      Letter.drawString("PUSH SHOT BUTTON", -4, -6.5f, 1);
  }

  public bool replayMode() {
    return _replayMode;
  }
}
