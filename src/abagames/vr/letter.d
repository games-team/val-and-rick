/*
 * $Id: letter.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.letter;

private import std.math;
private import opengl;
private import abagames.util.sdl.displaylist;
private import abagames.vr.screen;
private import abagames.vr.shape;

/**
 * Letters.
 */
public class Letter {
 private:
  static const float LETTER_WIDTH = 0.5f;
  static const float LETTER_HEIGHT = 0.5f;
  static BitmapShape[] whiteLetter;
  static BitmapShape[] yellowLetter;

  public static void init() {
    int i = 0;
    for (int y = 0; y < 3; y++) {
      for (int x = 0; x < 13; x++) {
        whiteLetter ~= new BitmapShape(BitmapShape.letters, x + y * 26, 0.5f);
        yellowLetter ~= new BitmapShape(BitmapShape.letters, x + y * 26 + 13, 0.5f);
        i++;
      }
    }
    whiteLetter ~= new BitmapShape(BitmapShape.ascii, 2 + 6 * 8, 0.5f);
  }

  public static void close() {
  }

  public static float getWidth(int n ,float s) {
    return n * s * LETTER_WIDTH;
  }

  public static float getHeight(float s) {
    return s * LETTER_HEIGHT;
  }

  private static void drawLetter(int n, float x, float y, float s, float d, int st) {
    glPushMatrix();
    glTranslatef(x, y, 0);
    if (s != 1)
      glScalef(s, s, 1);
    if (d != 0)
      glRotatef(d, 0, 0, 1);
    if (st == 0)
      whiteLetter[n].draw();
    else
      yellowLetter[n].draw();
    glPopMatrix();
  }

  private static void drawLetterRev(int n, float x, float y, float s, float d, int st) {
    glPushMatrix();
    glTranslatef(x, y, 0);
    if (s != 1)
      glScalef(s, -s, 1);
    if (d != 0)
      glRotatef(d, 0, 0, 1);
    if (st == 0)
      whiteLetter[n].draw();
    else
      yellowLetter[n].draw();
    glPopMatrix();
  }

  public static enum Direction {
    TO_RIGHT, TO_DOWN, TO_LEFT, TO_UP,
  }

  public static int convertCharToInt(char c) {
    int idx;
    if (c >= '0' && c <='9') {
      idx = c - '0';
    } else if (c >= 'A' && c <= 'Z') {
      idx = c - 'A' + 13;
    } else if (c >= 'a' && c <= 'z') {
      idx = c - 'a' + 13;
    } else if (c == '.') {
      idx = 12;
    } else if (c == '-') {
      idx = 10;
    } else if (c == '^') {
      idx = 11;
    } else if (c == '&') {
      idx = 39;
    }
    return idx;
  }

  public static void drawString(char[] str, float lx, float y, float s,
                                int d = Direction.TO_RIGHT, int cl = 0,
                                bool rev = false, float od = 0) {
    lx += LETTER_WIDTH * s / 2;
    y += LETTER_HEIGHT * s / 2;
    float x = lx;
    int idx;
    float ld;
    switch (d) {
    case Direction.TO_RIGHT:
      ld = 0;
      break;
    case Direction.TO_DOWN:
      ld = 90;
      break;
    case Direction.TO_LEFT:
      ld = 180;
      break;
    case Direction.TO_UP:
      ld = 270;
      break;
    }
    ld += od;
    foreach (char c; str) {
      if (c != ' ') {
        idx = convertCharToInt(c);
        if (rev)
          drawLetterRev(idx, x, y, s, ld, cl);
        else
          drawLetter(idx, x, y, s, ld, cl);
      }
      if (od == 0) {
        switch(d) {
        case Direction.TO_RIGHT:
          x += s * LETTER_WIDTH;
          break;
        case Direction.TO_DOWN:
          y += s * LETTER_WIDTH;
          break;
        case Direction.TO_LEFT:
          x -= s * LETTER_WIDTH;
          break;
        case Direction.TO_UP:
          y -= s * LETTER_WIDTH;
          break;
        }
      } else {
        x += cos(ld * PI / 180) * s * LETTER_WIDTH;
        y += sin(ld * PI / 180) * s * LETTER_WIDTH;
      }
    }
  }

  public static void drawNum(int num, float lx, float y, float s,
                             int d = Direction.TO_RIGHT, int cl = 0, int dg = 0) {
    lx += LETTER_WIDTH * s / 2;
    y += LETTER_HEIGHT * s / 2;
    int n = num;
    float x = lx;
    float ld;
    switch (d) {
    case Direction.TO_RIGHT:
      ld = 0;
      break;
    case Direction.TO_DOWN:
      ld = 90;
      break;
    case Direction.TO_LEFT:
      ld = 180;
      break;
    case Direction.TO_UP:
      ld = 270;
      break;
    }
    int digit = dg;
    for (;;) {
      drawLetter(n % 10, x, y, s, ld, cl);
      switch(d) {
      case Direction.TO_RIGHT:
        x -= s * LETTER_WIDTH;
        break;
      case Direction.TO_DOWN:
        y -= s * LETTER_WIDTH;
        break;
      case Direction.TO_LEFT:
        x += s * LETTER_WIDTH;
        break;
      case Direction.TO_UP:
        y += s * LETTER_WIDTH;
        break;
      }
      n /= 10;
      digit--;
      if (n <= 0 && digit <= 0)
        break;
    }
  }

  public static void drawNumSign(int num, float lx, float ly, float s, int st) {
    float dg;
    if (num < 100)
      dg = 2;
    else if (num < 1000)
      dg = 3;
    else if (num < 10000)
      dg = 4;
    else
      dg = 5;
    float x = lx + LETTER_WIDTH * s * dg / 2;
    float y = ly + LETTER_HEIGHT * s / 2;
    int n = num;
    for (;;) {
      drawLetterRev(n % 10, x, y, s, 0, st);
      x -= s * LETTER_WIDTH;
      n /= 10;
      if (n <= 0)
        break;
    }
  }

  /*public static void drawTime(int time, float lx, float y, float s, int st) {
    int n = time;
    if (n < 0)
      n = 0;
    float x = lx;
    for (int i = 0; i < 7; i++) {
      if (i != 4) {
        drawLetter(n % 10, x, y, s, Direction.TO_RIGHT, st);
        n /= 10;
      } else {
        drawLetter(n % 6, x, y, s, Direction.TO_RIGHT, st);
        n /= 6;
      }
      if ((i & 1) == 1 || i == 0) {
        switch (i) {
        case 3:
          drawLetter(41, x + s * 1.16f, y, s, Direction.TO_RIGHT, st);
          break;
        case 5:
          drawLetter(40, x + s * 1.16f, y, s, Direction.TO_RIGHT, st);
          break;
        default:
          break;
        }
        x -= s * LETTER_WIDTH;
      } else {
        x -= s * LETTER_WIDTH * 1.3f;
      }
      if (n <= 0)
        break;
    }
  }*/
}
