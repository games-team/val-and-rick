/*
 * $Id: shot.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.shot;

private import std.math;
private import std.string;
private import opengl;
private import abagames.util.actor;
private import abagames.util.vector;
private import abagames.util.rand;
private import abagames.vr.field;
private import abagames.vr.shape;
private import abagames.vr.screen;
private import abagames.vr.enemy;
private import abagames.vr.bulletactorpool;
private import abagames.vr.particle;
private import abagames.vr.ship;
private import abagames.vr.soundmanager;

/**
 * Player's shot.
 */
public class Shot: Actor {
 public:
  static const float SPEED = 0.6f;
 private:
  static BitmapShape shape;
  static Rand rand;
  Field field;
  EnemyPool enemies;
  BulletActorPool bullets;
  ParticlePool particles;
  Ship ship;
  Vector pos;
  int cnt;
  int _damage;
  float _deg;

  public static void init() {
    shape = new BitmapShape(BitmapShape.ship, 1);
    rand = new Rand;
  }

  public static void setRandSeed(long seed) {
    rand.setSeed(seed);
  }

  public static void close() {
    shape.close();
  }

  public override void init(Object[] args) {
    field = cast(Field) args[0];
    enemies = cast(EnemyPool) args[1];
    bullets = cast(BulletActorPool) args[2];
    particles = cast(ParticlePool) args[3];
    ship = cast(Ship) args[4];
    pos = new Vector;
  }

  public void set(Vector p, float d) {
    cnt = 0;
    pos.x = p.x;
    pos.y = p.y;
    _deg = d;
    _damage = 1;
    exists = true;
  }

  public void remove() {
    exists = false;
  }

  public override void move() {
    pos.x += sin(_deg) * SPEED;
    pos.y += cos(_deg) * SPEED;
    if ((!ship.airMode && field.getBlock(pos) >= 0) ||
        !field.checkInOuterField(pos) || pos.y > field.size.y)
      remove();
    enemies.checkShotHit(pos, shape, this);
    cnt++;
  }

  public override void draw() {
    glPushMatrix();
    Screen.glTranslate(pos);
    glRotatef(-_deg * 180 / PI, 0, 0, 1);
    shape.draw();
    glPopMatrix();
  }

  public int damage() {
    return _damage;
  }

  public float deg() {
    return _deg;
  }
}

public class ShotPool: ActorPool!(Shot) {
  public this(int n, Object[] args) {
    super(n, args);
  }
}

public class Blaster {
 public:
  bool exists;
 private:
  static const int IMPACT_DURATION = 45;
  static const float HEIGHT_INC = 0.02f;
  Field field;
  EnemyPool enemies;
  Vector pos;
  Vector sightPos;
  float my;
  ResizableDrawable shape;
  BitmapShape sightShape;
  float z;
  int cnt;

  public this(Field field, EnemyPool enemies) {
    this.field = field;
    this.enemies = enemies;
    shape = new ResizableDrawable;
    shape.shape = new BitmapShape(BitmapShape.sight, 2);
    shape.size = 0;
    sightShape = new BitmapShape(BitmapShape.sight, 1);
    pos = new Vector;
    sightPos = new Vector;
    exists = false;
  }

  public void set(Vector p, float sightRange) {
    pos.x = p.x;
    pos.y = p.y;
    sightPos.x = p.x;
    sightPos.y = p.y + sightRange;
    z = 0;
    cnt = IMPACT_DURATION;
    my = sightRange / cnt;
    exists = true;
    SoundManager.playSe("blaster.wav");
  }

  public void move() {
    cnt--;
    if (cnt > IMPACT_DURATION / 2)
      z += HEIGHT_INC;
    else
      z -= HEIGHT_INC;
    pos.y += my;
    pos.y -= field.lastScrollY;
    sightPos.y -= field.lastScrollY;
    if (cnt < 0) {
      exists = false;
      shape.size = 1.2f;
      enemies.checkShotHit(pos, shape);
    }
    shape.size = 1 + z;
  }

  public void draw() {
    glPushMatrix();
    Screen.glTranslate(pos);
    shape.draw();
    glPopMatrix();
    glPushMatrix();
    Screen.glTranslate(sightPos);
    sightShape.draw();
    glPopMatrix();
  }
}
