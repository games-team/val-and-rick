/*
 * $Id: replay.d,v 1.1.1.1 2005/03/13 16:15:04 kenta Exp $
 *
 * Copyright 2005 Kenta Cho. Some rights reserved.
 */
module abagames.vr.replay;

private import std.stream;
private import abagames.util.sdl.recordableinput;
private import abagames.util.sdl.pad;

/**
 * Manage a replay data.
 */
public class ReplayData {
 public:
  static const char[] dir = "replay";
  static const int VERSION_NUM = 10;
  InputRecord!(PadState) inputRecord;
  long seed;
 private:

  public void save(char[] fileName) {
    auto File fd = new File;
    fd.create(dir ~ "/" ~ fileName);
    fd.write(VERSION_NUM);
    fd.write(seed);
    inputRecord.save(fd);
    fd.close();
  }

  public void load(char[] fileName) {
    auto File fd = new File;
    fd.open(dir ~ "/" ~ fileName);
    int ver;
    fd.read(ver);
    if (ver != VERSION_NUM)
      throw new Error("Wrong version num");
    fd.read(seed);
    inputRecord = new InputRecord!(PadState);
    inputRecord.load(fd);
    fd.close();
  }
}
