Val & Rick  readme.txt
for Windows98/2000/XP(OpenGL required)
ver. 0.1 alpha
(C) Kenta Cho

Annihilate the enemies flying around space!
But please excuse the alpha quality of Val & Rick.


Usage Instructions

Player movement		arrow keys, numeric keypad, W/A/S/D, joystick

Shot			Z, Left Control, ., joystick buttons 1/4/5/8

Hold down for rapid fire

Blaster/Fixed direction	X, Left Alt, Left Shift, /, joystick buttons 2/3/6/7

The blaster destroys the enemy under the target in front of the player.
Use it to destroy the bosses located on the floating asteroids.

Pause			P


How to play

At the title screen, press a shot key to start the game.
Press the escape key the end the game.

Advance the player (Val) toward the top of the screen to destroy enemies.
Destroy the enemies and grab the crystals.  After collecting a certain
number of crystals, a friendly ship (Rick) will enter the game.  Combine
with the friendly ship, and until the crystals are used up, you can fly
through space.  Enemy bullets cannot hit you, and you earn points.

When the player is destroyed, the game is over.


Options

The following options may be set.

 -brightness n  Set the brightness (n = 0 - 100, default 100)
 -luminosity n  Set the luminosity (n = 0 - 100, default 0)
 -res x y       Set the resolution
 -nosound       Disable the sound
 -window        Activate windowed mode
 -reverse       Swap shot and blaster keys


Suggestions, thoughts

Please send comments etc. to cs8k-cyu@asahi-net.or.jp


Thanks

2Chan Game Development Board, Shooter Game Development Collaborators
631 other people using this for bitmap creation.

Val & Rick was written in the D language.
 The D Programming Language
 http://www.kmonos.net/alang/d/

libBulletML is used to parse the BulletML files
 libBulletML
 http://shinh.skr.jp/libbulletml/

Simple DirectMedia Layer is used for media handling.
 Simple DirectMedia Layer
 http://www.libsdl.org/

SDL_mixer and the Ogg Vorbis codec are used for background music and sound effects.
 SDL_mixer 1.2
 http://www.libsdl.org/projects/SDL_mixer/
 Vorbis.com
 http://www.vorbis.com/

The OpenGL, SDL, and SDL_mixer header files from D - porting are used
 D - porting
 http://shinh.skr.jp/d/porting.html

The Mersenne Twister random number generator is used
 Mersenne Twister: A random number generator (since 1997/10)
 http://www.math.sci.hiroshima-u.ac.jp/‾m-mat/MT/mt.html


History

2005  3/14  ver. 0.1 alpha
            Alpha Version


License

Val & Rick is distributed under a BSD style license.

License
-------

Copyright 2005 Kenta Cho. All rights reserved. 

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided that 
the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer. 

 2. Redistributions in binary form must reproduce the above copyright notice, 
    this list of conditions and the following disclaimer in the documentation 
    and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
