Val & Rick  readme.txt
for Windows98/2000/XP(要OpenGL)
ver. 0.1 alpha
(C) Kenta Cho

地を駆け空を飛び敵を殲滅せよ。
まだアルファ版なのでいろいろ大目に見てください、Val & Rick。


○ インストール方法

vr0_1a.zipを適当なフォルダに展開してください。
その後、'vr.exe'を実行してください。


○ 操作方法

- 自機の移動            矢印キー, テンキー, [WASD] / ジョイステック

- ショット              [Z][左Ctrl][.]             / トリガ1, 4, 5, 8

押しっぱなしで連射されます。

- ブラスター / 方向固定 [X][左Alt][左Shift][/]     / トリガ2, 3, 6, 7

ブラスターは自機上方の照準に向かって飛び、敵を破壊します。
主に浮遊大陸上の敵の破壊に使います。

- ポーズ            [P]


○ 遊び方

タイトル画面でショットキーを押すとゲームを開始します。
エスケープキーを押すとゲームを終了します。

自機(Val)を操作し敵を破壊し、画面上方へできるだけ進んでください。
敵を破壊するとクリスタルを手に入れることができます。
クリスタルが一定数になると友軍機(Rick)が出現します。
友軍機と合体するとクリスタルが尽きるまでの間空を飛ぶことができます。
敵弾にあたらないようにして、距離を稼ぎましょう。

自機がすべて破壊されるとゲームオーバーです。


○ オプション

以下のオプションが指定できます。

 -brightness n  画面の明るさを指定します。(n = 0 - 100, デフォルト100)
 -luminosity n  発光エフェクトの強さを指定します。(n = 0 - 100, デフォルト0)
 -res x y       画面の解像度を(x, y)にします。
 -nosound       音を出力しません。
 -window        ウィンドウモードで起動します。
 -reverse       ショットとブラスターのキーを入れ替えます。


○ ご意見、ご感想

コメントなどは、cs8k-cyu@asahi-net.or.jp までお願いします。


○ 謝辞

2chゲーム製作技術板シューティングゲーム製作技術総合スレ
631氏ほか作成のビットマップを利用しています。

Val & RickはD言語(ver. 0.118)で書かれています。
 プログラミング言語D
 http://www.kmonos.net/alang/d/

BulletMLファイルのパースにlibBulletMLを利用しています。
 libBulletML
 http://shinh.skr.jp/libbulletml/

メディアハンドリングにSimple DirectMedia Layerを利用しています。
 Simple DirectMedia Layer
 http://www.libsdl.org/

BGMとSEの出力にSDL_mixerとOgg Vorbis CODECを利用しています。
 SDL_mixer 1.2
 http://www.libsdl.org/projects/SDL_mixer/
 Vorbis.com
 http://www.vorbis.com/

D - portingのOpenGL, SDL, SDL_mixerヘッダファイルを利用しています。
 D - porting
 http://shinh.skr.jp/d/porting.html

乱数発生器にMersenne Twisterを利用しています。
 Mersenne Twister: A random number generator (since 1997/10)
 http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/mt.html


○ ヒストリ

2005  3/14  ver. 0.1 alpha
            アルファバージョン


○ ライセンス

Val & RickはBSDスタイルライセンスのもと配布されます。

License
-------

Copyright 2005 Kenta Cho. All rights reserved. 

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided that 
the following conditions are met: 

 1. Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer. 

 2. Redistributions in binary form must reproduce the above copyright notice, 
    this list of conditions and the following disclaimer in the documentation 
    and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
